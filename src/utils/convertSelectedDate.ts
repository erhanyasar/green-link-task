export default function convertSelectedValue(date: Date): string {
  const _date = date.toLocaleDateString().replaceAll(".", "-");
  const _day = _date.substring(0, _date.indexOf("-"));
  const _month = _date
    .substring(_date.indexOf("-") + 1)
    .substring(0, _date.indexOf("-"));
  const _year = _date
    .substring(_date.indexOf("-") + 1)
    .substring(_date.indexOf("-") + 1);

  return `${_year}-${_month}-${_day}`;
}
