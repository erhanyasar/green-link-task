declare global {
  interface Date {
    isValid(date: Date): boolean;
  }
}

export default function isValidDate(param: Date[]): boolean[] {
  const [start, end] = param;
  const startDate = new Date(start),
    endDate = new Date(end);

  // Since given brief describes as one the dates are invalid
  if (start !== null && end !== null) {
    Date.prototype.isValid = (date: Date): boolean => {
      // Also possible to return date.getTime() === date.getTime();
      return !Number.isNaN(date.getTime());
    };
  }
  return [Date.prototype.isValid(startDate), Date.prototype.isValid(endDate)];
}
