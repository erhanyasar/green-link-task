import convertSelectedDate from "./convertSelectedDate";

const url: URL = new URL(location.href);
const searchParams: URLSearchParams = new URLSearchParams(url.search);

export const syncURL = (start: Date, end: Date) => {
  searchParams.delete("start");
  searchParams.delete("end");
  searchParams.set("start", convertSelectedDate(start));
  searchParams.set("end", convertSelectedDate(end));
  url.search = searchParams.toString();
  location.replace(url);
};
