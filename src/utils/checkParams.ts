import { ParamsProp } from "../types";
import isValidDate from "./isValidDate";

export default function checkParams(start: Date, end: Date): ParamsProp {
  const currentDate = new Date(),
    yesterday = new Date(currentDate),
    firstDayOfMonth = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      1
    );

  yesterday.setDate(yesterday.getDate() - 1);

  if (
    (!start && !end) ||
    isValidDate([start, end])[0] === false ||
    isValidDate([start, end])[1] === false
  ) {
    return {
      start: firstDayOfMonth,
      end: yesterday,
    };
  } else if (currentDate === firstDayOfMonth) {
    return {
      start: new Date(yesterday.getFullYear(), yesterday.getMonth(), 1),
      end: yesterday,
    };
  } else
    return {
      start: new Date(start),
      end: new Date(end),
    };
}
