export type ParamsProp = {
  start: Date;
  end: Date;
};

export type GlobalStateProp = {
  defaultPeriod: DefaultPeriodSliceProp;
};

export type DefaultPeriodSliceProp = {
  dates: (Date | null)[];
};
