import { createSlice } from "@reduxjs/toolkit";
import { DefaultPeriodSliceProp } from "../../../types";

const initialState: DefaultPeriodSliceProp = {
  dates: [null, null],
};

export const defaultPeriodSlice = createSlice({
  name: "defaultPeriod",
  initialState,
  reducers: {
    setDefaultPeriod: (state, action) => {
      state = {
        dates: [new Date(action.payload), new Date()],
      };
    },
  },
});

export const { setDefaultPeriod } = defaultPeriodSlice.actions;
export default defaultPeriodSlice.reducer;
