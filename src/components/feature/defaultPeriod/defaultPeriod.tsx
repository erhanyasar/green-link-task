import { useState, useEffect } from "react";
import checkParams from "../../../utils/checkParams";
import { syncURL } from "../../../utils/syncURL";
import DatePicker from "react-datepicker";
import { useSelector, useDispatch } from "react-redux";
import { GlobalStateProp } from "../../../types";
import { setDefaultPeriod } from "./defaultPeriodSlice";
import "react-datepicker/dist/react-datepicker.css";

const url: URL = new URL(location.href);
const searchParams: URLSearchParams = new URLSearchParams(url.search);

export const DefaultPeriod = () => {
  const { dates } = useSelector(
    (state: GlobalStateProp) => state.defaultPeriod
  );
  const dispatch = useDispatch();

  const [startDate, setStartDate] = useState<Date>(
    new Date(searchParams.get("start") || "")
  );
  const [endDate, setEndDate] = useState<Date>(
    new Date(searchParams.get("end") || "")
  );

  const handleDateSelect = (selected: Date) => {
    setStartDate(selected);
    syncURL(selected, endDate);
    dispatch(setDefaultPeriod(selected.toDateString())); // TODO: Implement
  };

  useEffect(() => {
    /* Since it's given via brief documentation to be
       "Set  defaultPeriod  function when the app is ready" */
    const { start, end } = checkParams(startDate, endDate);
    setStartDate(start);
    setEndDate(end);
  }, []);

  return (
    <>
      <DatePicker
        showIcon
        selected={startDate}
        selectsStart
        startDate={startDate}
        endDate={endDate}
        onSelect={handleDateSelect}
        onChange={(date) => setStartDate(date || new Date())}
      />
    </>
  );
};
