import { configureStore } from "@reduxjs/toolkit";
import defaultPeriodReducer from "../feature/defaultPeriod/defaultPeriodSlice";

export const store = configureStore({
  reducer: {
    defaultPeriod: defaultPeriodReducer,
  },
});
