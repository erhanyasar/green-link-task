import { DefaultPeriod } from "../feature/defaultPeriod/defaultPeriod";
import "./App.css";

function App() {
  return <DefaultPeriod />;
}

export default App;
